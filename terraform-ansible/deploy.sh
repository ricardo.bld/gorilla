#!/bin/bash

set -e
set -x

# ensure SSH agent has all keys
ssh-add -k ~/.ssh/key.pem

# set up the infrastructure
cd terraform-jenkins
terraform init -backend=true
terraform apply -auto-approve


#cd ../terraform-jenkins
terraform output
