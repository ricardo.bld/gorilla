output "jenkins_public_dns" {
  value = "[ ${aws_instance.jenkins-master.public_dns} ]"
}

output "address" {
  value = "${aws_instance.jenkins-master.public_ip}"
}

output "ssh" {
  value = "ssh ubuntu@${aws_instance.jenkins-master.public_ip}"
}
