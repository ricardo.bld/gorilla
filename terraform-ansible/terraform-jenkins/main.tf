terraform {
  backend "s3" {
    bucket  = "jenkins-store.devlopment-cr"
    key     = "jenkins.terraform.tfstate"
    region  = "us-east-1"
    profile   = "development-cr"
  }
}

provider "aws" {
  region = "${var.region}"
  profile   = "development-cr"
}

#Create IAM Role for VM
resource "aws_iam_role" "jenkins_role" {
  name               = "jenkins_role"
  assume_role_policy = "${file("assume-role-policy.json")}"
}

resource "aws_iam_policy" "jenkins_create_policy" {
    name = "jenkins_policy"
    path = "/"
    policy = "${file("ecs-policy.json")}"
}
resource "aws_iam_policy_attachment" "jenkins_policy-attach" {
  name       = "jenkins-attachment"
  roles      = ["${aws_iam_role.jenkins_role.name}"]
  policy_arn = "${aws_iam_policy.jenkins_create_policy.arn}"
}

resource "aws_iam_instance_profile" "jenkins_profile" {
  name  = "jenkins_profile"
  roles = ["${aws_iam_role.jenkins_role.name}"]
}

#Create IAM Role for Fargate
resource "aws_iam_role" "fargate_role" {
  name               = "${var.role_name}"
  assume_role_policy = "${file("assume-role-ecs.json")}"
}

resource "aws_iam_policy" "fargate_create_policy" {
    name = "fargate_policy"
    path = "/"
    policy = "${file("ecs-policy.json")}"
}

resource "aws_iam_policy_attachment" "fargate_policy-attach" {
  name       = "fargate-attachment"
  roles      = ["${aws_iam_role.fargate_role.name}"]
  policy_arn = "${aws_iam_policy.fargate_create_policy.arn}"
}

#We create the Security Group to allow access to Jenkins
resource "aws_security_group" "sg_jenkins" {
  name = "sg_${var.jenkins_name}"
  description = "Allows all traffic"

  # SSH
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTP
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # HTTPS
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  # Jenkins JNLP port
  ingress {
    from_port = 50000
    to_port = 50000
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

#Creates the EC2 instance in AWS
resource "aws_instance" "jenkins-master" {
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.sg_jenkins.name}"]
  ami = "${lookup(var.amis, var.region)}"
  key_name = "${var.key_name}"
  root_block_device{
    volume_type = "gp2"
    volume_size = "50"
  }
  iam_instance_profile   = "${aws_iam_instance_profile.jenkins_profile.name}"

  tags {
    "Name" = "${var.jenkins_name}"
  }

  provisioner "local-exec" {
   command = "echo \"[${var.jenkins_name}]\n${aws_instance.jenkins-master.public_ip}\" > ${var.jenkins_name}"
 }
 provisioner "local-exec" {
  command = "sleep 60;ansible-playbook --extra-vars 'host_key_checking=false jenkins_host=${aws_instance.jenkins-master.public_dns}' -i ${var.jenkins_name} ../ansible/provision-jenkins.yml"
}
}
