variable "region" {
  description = "The AWS region to create resources in."
  default = "us-east-1"
}

variable "availability_zone" {
  description = "The availability zone"
  default = "us-east-1a"
}

variable "jenkins_name" {
  description = "The name of the Jenkins server."
  default = "jenkins-master"
}

variable "amis" {
  description = "Which AMI to spawn. Defaults to the Ubuntu 18.04  AMIs: https://aws.amazon.com/marketplace/pp/B07CQ33QKV."
  default = {
    us-east-1 = "ami-6061141f"
    us-east-2 = "ami-d82e11bd"
    us-west-1 = "ami-e3293383"
    us-west-2 = "ami-6550151d"
    eu-west-1 = "ami-1025aa63"
    eu-central-1 = "ami-c61f2b2d"
  }
}

variable "instance_type" {
  default = "t2.medium"
}

variable "policy_arn" {
  default = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

variable "role_name" {
  default = "fargate_Execution"
}

variable "key_name" {
  default = "key-name"
  description = "SSH key name in your AWS account for AWS instances."
}

variable "s3_bucket" {
  default = "jenkins-store.devlopment-cr"
  description = "S3 bucket where remote state and Jenkins data will be stored."
}
