#!/usr/bin/env bash
#
#

set -e
set -x


# Check that environment variables are set from Jenkins
if [ -z "$ecr_repository" ]; then
    echo "*** No AWS ECR respository supplied in ENV ecr_repository, exiting... ****"
    exit 1
fi

if [ -z "$ecr_region" ]; then
    echo "*** No AWS ECR Region supplied in ENV ecr_region, exiting... ****"
    exit 1
fi

#  Set up paths
AWS="/var/jenkins_home/.local/bin/aws --region $ecr_region"

DOCKER_IMAGE=$1

# Step 1 Get login  snippet from AWS
DOCKER_LOGIN=$($AWS ecr get-login --no-include-email)

# Disable output as we have passwords here
set +x

# Step 2 Execute docker login against AWS.
eval $DOCKER_LOGIN

# Step 3 Tag image with AWS Specifics
docker tag $DOCKER_IMAGE $ecr_repository

# Tag with build release so we can track it
docker tag $DOCKER_IMAGE $ecr_repository:$JOB_NAME.$BUILD_NUMBER

# Step 4 Push docker image to ECR
docker push $ecr_repository
